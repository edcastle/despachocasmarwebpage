const gulp = require('gulp');
const pug = require('gulp-pug');

//const carpeta_actual = "entregableE2";

gulp.task("vistas", () => {
	return gulp
	  .src(`./src/views/*.pug`)
	  .pipe(pug({
		  pretty: true
	  }))
	  .pipe(gulp.dest(`./public/`));
  });
  
gulp.task('default', () => {
	gulp.watch(`./src/views/*.pug`, gulp.series('vistas'))
})